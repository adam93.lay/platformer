﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceManager : MonoBehaviour
{
  public Transform UI;

  // Prefabs
  public GameObject HeartPrefab;
  public GameObject HeartHalfPrefab;
  public GameObject HeartEmptyPrefab;

  // UI Elements
  private Transform _healthBar;
  private Transform _inventory;
  private TextMeshProUGUI _scoreText;

  private void Awake()
  {
    _scoreText = UI.Find("ScoreText").GetComponent<TextMeshProUGUI>();
    _healthBar = UI.Find("HealthBar");
    _inventory = UI.Find("Inventory");

    Game.Instance.OnScoreChanged += score => _scoreText.text = "Score: " + score;

    PlayerController.Instance.OnHealthChanged += (hp, max) =>
    {
      // Remove hearts
      RemoveChildren(_healthBar);

      // Add full health hearts
      for (int i = 0; i < (hp / 2); i++)
        Instantiate(HeartPrefab, _healthBar);

      // Add half health heart if necessary
      if (hp % 2 == 1)
        Instantiate(HeartHalfPrefab, _healthBar);

      // Add empty hearts
      for (int i = 0; i < ((max - hp) / 2); i++)
        Instantiate(HeartEmptyPrefab, _healthBar);
    };

    PlayerController.Instance.OnInventoryChanged += inv =>
    {
      RemoveChildren(_inventory);

      foreach (ICollectableItem item in inv)
      {
        GameObject icon = GenerateInventoryIcon(item);
        icon.transform.SetParent(_inventory);
      }
    };
  }

  private GameObject GenerateInventoryIcon(ICollectableItem item)
  {
    var go = new GameObject();

    var rect = go.AddComponent<RectTransform>();
    rect.sizeDelta = new Vector2(64, 64);

    var img = go.AddComponent<Image>();
    img.sprite = item.GetSprite();

    return go;
  }

  private void RemoveChildren(Transform t)
  {
    for (int i = t.childCount - 1; i >= 0; i--)
      Destroy(t.GetChild(i).gameObject);
  }
}
