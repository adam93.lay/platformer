﻿using UnityEngine;

public interface ICollectableItem
{
  Sprite GetSprite();
}

public class CollectibleItem : MonoBehaviour, ICollectableItem
{
  private void OnTriggerEnter2D(Collider2D col)
  {
    var player = col.gameObject.GetComponent<PlayerController>();
    if (player == null)
      return;

    gameObject.SetActive(false);

    player.AddInventoryItem(this);
  }

  public Sprite GetSprite()
  {
    return GetComponent<SpriteRenderer>().sprite;
  }
}
