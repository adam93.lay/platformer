﻿using UnityEngine;

public class Rotator : MonoBehaviour
{
  public float Speed = 1;

  private void Update()
  {
    transform.Rotate(Vector3.back, Time.deltaTime * Speed);
  }
}
