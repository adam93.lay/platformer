﻿using UnityEngine;

public class HealthPickup : MonoBehaviour
{
  private void OnTriggerEnter2D(Collider2D col)
  {
    var player = col.gameObject.GetComponent<PlayerController>();
    if (player == null || player.Health >= player.MaxHealth)
      return;

    // Increment health
    player.Health++;

    // Spawn effect
    transform.Find("OnDestroyEffect").GetComponent<ParticleSystem>().Play();
    // Stop rendering sprite
    Destroy(GetComponent<SpriteRenderer>());
    // Stop colliding
    Destroy(GetComponent<CircleCollider2D>());
    // Destroy self after particles are done
    Destroy(gameObject, 1.5f);
  }
}
