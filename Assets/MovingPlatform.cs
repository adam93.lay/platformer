﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
  public Vector2 StartPoint;
  public Vector2 EndPoint;
  public float Speed;

  private Vector2 _moveStart;
  private Vector2 _target;
  private float _lerpTime;
  private float _currentLerpTime;

  private void Start()
  {
    _lerpTime = Vector2.Distance(StartPoint, EndPoint);
  }

  private void Update()
  {
    if ((Vector2)transform.position == StartPoint)
    {
      _moveStart = StartPoint;
      _target = EndPoint;
      _currentLerpTime = 0;
    }
    else if ((Vector2)transform.position == EndPoint)
    {
      _moveStart = EndPoint;
      _target = StartPoint;
      _currentLerpTime = 0;
    }

    _currentLerpTime = Mathf.Clamp(_currentLerpTime + Time.deltaTime * Speed, 0, _lerpTime);

    float x = Mathf.SmoothStep(_moveStart.x, _target.x, _currentLerpTime / _lerpTime);
    float y = Mathf.SmoothStep(_moveStart.y, _target.y, _currentLerpTime / _lerpTime);
    transform.position = new Vector2(x, y);
  }

  private Transform _originalParent;

  private void OnCollisionEnter2D(Collision2D col)
  {
    _originalParent = col.transform.parent;
    col.transform.parent = transform;
  }

  private void OnCollisionExit2D(Collision2D col)
  {
    col.transform.parent = _originalParent;
    _originalParent = null;
  }
}
