﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
  public static Game Instance;

  private int _score;
  public int Score
  {
    get => _score;
    set
    {
      _score = value;
      OnScoreChanged?.Invoke(_score);
    }
  }
  public Action<int> OnScoreChanged;

  private void Awake()
  {
    Instance = this;
  }

  private void Start()
  {
    PlayerController.Instance.OnHealthChanged += (hp, _) =>
    {
      if (hp <= 0)
      {
        GameOver();
      }
    };
  }

  private void GameOver()
  {
    SceneManager.LoadScene("MainScene");
  }
}
