﻿using UnityEngine;

public class Hazard : MonoBehaviour
{
  public int Damage = 1;

  private void OnTriggerEnter2D(Collider2D col)
  {
    var player = col.gameObject.GetComponent<PlayerController>();
    if (player == null)
      return;

    player.Health -= Damage;
  }
}
