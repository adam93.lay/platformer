﻿using UnityEngine;

public class Portal : MonoBehaviour
{
  public Portal Partner;
  public bool DisableNextTrigger;

  private void OnTriggerEnter2D(Collider2D col)
  {
    if (DisableNextTrigger)
    {
      DisableNextTrigger = false;
      return;
    }

    var player = col.gameObject.GetComponent<PlayerController>();
    if (player == null)
      return;

    // One way portal
    if (Partner == null)
      return;

    Partner.DisableNextTrigger = true;

    // Move player to partner
    player.transform.position = Partner.transform.position;
  }
}
