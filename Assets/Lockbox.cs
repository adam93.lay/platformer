﻿using System.Collections;
using System.Linq;
using UnityEngine;

public class Lockbox : MonoBehaviour
{
  public KeyColours RequiredKey;
  public GameObject ContentPrefab;

  private bool _opened;

  private void OnTriggerEnter2D(Collider2D col)
  {
    // Return if already open
    if (_opened)
      return;

    var player = col.gameObject.GetComponent<PlayerController>();
    if (player == null)
      return;

    // Get key of matching colour from player's inventory
    ICollectableItem key = player.Inventory
      .FirstOrDefault(item => item is Key k && k.KeyColour == KeyColours.Green);

    // If there's no key then won't open!
    if (key == null)
      return;

    // Remove/Delete the key
    player.RemoveInventoryItem(key);
    Destroy(((Key)key).gameObject);

    // "Open" the box
    StartCoroutine(RaiseContents());
    transform.Find("OnOpenEffect").GetComponent<ParticleSystem>().Play();

    // Set the box as open
    _opened = true;
  }

  private float _lerpTime = 1.0f;
  private float _currentLerpTime;
  private IEnumerator RaiseContents()
  {
    GameObject content = Instantiate(ContentPrefab, transform);
    var contentCollider = content.GetComponent<Collider2D>();
    contentCollider.enabled = false;

    while (_currentLerpTime < _lerpTime)
    {
      _currentLerpTime = Mathf.Clamp(_currentLerpTime + Time.deltaTime, 0, _lerpTime);

      content.transform.localPosition = new Vector2(0, Mathf.SmoothStep(0, 1, _currentLerpTime / _lerpTime));

      yield return null;
    }

    contentCollider.enabled = true;
  }
}
