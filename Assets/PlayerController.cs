﻿using System;
using System.Collections.Generic;
using UnityEditor.Animations;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
  public static PlayerController Instance;
  // Public
  public float Speed;
  public float JumpForce;
  public int MaxJumps = 2;
  public int MaxHealth = 10;
  public Transform GroundCheck;
  public float CheckRadius;
  public LayerMask GroundLayer;

  // Private
  private float _moveInput;
  private bool _grounded;
  private int _extraJumps;
  private Rigidbody2D _rb;
  private SpriteRenderer _sr;
  private Animator _animator;
  private ParticleSystem _onDamageEffect;

  // Properties
  /// <summary>
  /// Current Health, Max Health
  /// </summary>
  public Action<int, int> OnHealthChanged;
  [SerializeField]
  private int _health = 10;
  public int Health
  {
    get => _health;
    set
    {
      // Play damage effect if HP has gone down
      if (value < _health)
        _onDamageEffect.Play();

      // Adjust HP
      _health = value;

      // Notify
      OnHealthChanged?.Invoke(_health, MaxHealth);
    }
  }

  #region Inventory
  public List<ICollectableItem> Inventory { get; } = new List<ICollectableItem>();
  public Action<List<ICollectableItem>> OnInventoryChanged;

  public void AddInventoryItem(ICollectableItem item)
  {
    Inventory.Add(item);
    OnInventoryChanged?.Invoke(Inventory);
  }

  public void RemoveInventoryItem(ICollectableItem item)
  {
    Inventory.Remove(item);
    OnInventoryChanged?.Invoke(Inventory);
  }
  #endregion

  private void Awake()
  {
    Instance = this;
    _onDamageEffect = transform.Find("OnDamageEffect").GetComponent<ParticleSystem>();
  }

  private void Start()
  {
    _rb = GetComponent<Rigidbody2D>();
    _sr = GetComponent<SpriteRenderer>();
    _animator = GetComponent<Animator>();

    Health = MaxHealth;
  }

  private void FixedUpdate()
  {
    // Get user input
    _moveInput = Input.GetAxisRaw("Horizontal");

    _grounded = Physics2D.OverlapCircle(GroundCheck.position, CheckRadius, GroundLayer);
    _rb.velocity = new Vector2(_moveInput * Speed, _rb.velocity.y);

    // Sneaky pete
    _rb.gravityScale = Input.GetKey(KeyCode.LeftShift) ? 1.5f : 3f;
  }

  private void Update()
  {
    if (_grounded)
      _extraJumps = MaxJumps;

    // Set Animation
    _animator.SetTrigger(Mathf.Abs(_moveInput) > 0.5 ? "Walking" : "Idling");

    // Flip sprite if changing direction
    _sr.flipX = _moveInput == 0 ? _sr.flipX : _moveInput < 0;

    if (Input.GetKeyDown(KeyCode.Space))
    {
      Jump();
    }
  }

  private void Jump()
  {
    if (_extraJumps <= 0)
      return;

    _rb.velocity = Vector2.up * JumpForce;
    _extraJumps--;
  }
}
