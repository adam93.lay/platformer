﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class Score : MonoBehaviour
{
  public int ScoreValue = 1;

  private ParticleSystem _onDeathEffect;

  private void Start()
  {
    _onDeathEffect = transform.Find("OnDestroyEffect").GetComponent<ParticleSystem>();
  }

  private void OnCollisionEnter2D(Collision2D col)
  {
    // Spawn effect
    _onDeathEffect.Play();

    // Increment score
    Game.Instance.Score += ScoreValue;

    // Stop rendering sprite
    Destroy(GetComponent<SpriteRenderer>());
    // Stop colliding
    Destroy(GetComponent<CircleCollider2D>());
    // Destroy self after particles are done
    Destroy(gameObject, 1.5f);
  }
}
